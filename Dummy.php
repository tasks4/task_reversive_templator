<?php

class Dummy
{
    /**
     * @var string
     */
    public $openBracket = '{';

    /**
     * @var string
     */
    public $closeBracket = '}';

    /**
     * @param $source
     * @param array $variables
     * @return array|string|string[]
     * @throws Exception
     */
    public function run($source, array $variables)
    {
        if (!$this->checkVariables($source)) {
            return $source;
        }

        $nameVariables = $this->buildVariableForTemplateVersion2($variables);

        $valueVariables =
            array_map(
                function ($val) {
                    return htmlspecialchars($val);
                },
                array_values($variables)
            );

        if (!$this->checkBrackets($source)) {
            throw new Exception('Invalid template');
        }

        return str_replace($nameVariables, $valueVariables, $source);
    }


    /**
     * @param $string
     * @return bool
     */
    private function checkBrackets($string): bool
    {
        $string = str_split($string);
        $stack = [];

        foreach ($string as $key => $value) {
            switch ($value) {
                case $this->openBracket:
                    array_push($stack, $this->openBracket);
                    break;

                case $this->closeBracket:
                    if (array_pop($stack) !== $this->openBracket)
                        return false;
                    break;
            }
        }

        return (empty($stack));
    }

    /**
     * @param $source
     * @return bool
     */
    private function checkVariables($source): bool
    {
        $pattern = sprintf('~\%s|\%s~sui', $this->openBracket, $this->closeBracket);

        return (preg_match($pattern, $source));
    }

    /**
     * @param $variables
     * @return array
     */
    private function buildVariableForTemplate($variables): array
    {
        $openTags = sprintf('%s%1$s', $this->openBracket);
        $closeTags = sprintf('%s%1$s', $this->closeBracket);
        $separator = sprintf('%s,%s', $closeTags, $openTags);

        return
            explode(
                ',',
                sprintf(
                    '%s%s%s',
                    $openTags,
                    implode($separator, array_keys($variables)),
                    $closeTags
                )
            );
    }

    /**
     * @param $variables
     * @return array
     */
    private function buildVariableForTemplateVersion2($variables): array
    {
        $openTags = sprintf('%s%1$s', $this->openBracket);
        $closeTags = sprintf('%s%1$s', $this->closeBracket);

        return
            array_map(
                function ($val) use ($openTags, $closeTags) {
                    return sprintf("%s%s%s", $openTags, $val, $closeTags);
                },
                array_keys($variables)
            );
    }
}

$name = 'Juni';
$name2 = '<robot>';

$dummy = new Dummy();
//$dummy->openBracket = '[';
//$dummy->closeBracket = ']';

echo
$dummy->run(
    file_get_contents('template.php'),
    compact('name', 'name2')
);

